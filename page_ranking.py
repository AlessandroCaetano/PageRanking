#!/usr/bin/env python3

'''
usage ./page_ranking or python page_ranking.py
'''
########################################
##  Implementation of text analysis of issues and commentaries from git repositories. This script is modularized on the following modules:
##
##    1. Information Extraction: Make a resquest for gitlab api to get all issues
##    on the repo, then all the comments are retrieved.
##
##    2. Generating Valid Structure: After the information retrieval, a dictionary
##    is generated with issues numbers and the issues that are linked to it on the comments.
##    Then, a networkx directed acyclic graph is generated, to make easier to run
##    pageranking on the graph.
##
##    3. The final part is run the page ranking algorithm, it is defined as:
##
##    pagerank(G, alpha=0.85, personalization=None, max_iter=100, tol=1e-06,
##    nstart=None, weight='weight', dangling=None)
##
##    G (graph) – A NetworkX graph. Undirected graphs will be converted to a
##    directed graph with two directed edges for each undirected edge.
##
##    alpha (float, optional) – Damping parameter for PageRank, default=0.85.
##
##    personalization (dict, optional) – The “personalization vector” consisting
##    of a dictionary with a key for every graph node and nonzero personalization
##    value for each node. By default, a uniform distribution is used.
##
##    max_iter (integer, optional) – Maximum number of iterations in power method
##    eigenvalue solver.
##
##    tol (float, optional) – Error tolerance used to check convergence in power
##    method solver.
##
##    nstart (dictionary, optional) – Starting value of PageRank iteration for each node.
##
##    weight (key, optional) – Edge data key to use as weight. If None weights are set to 1.
##
##    dangling (dict, optional) – The outedges to be assigned to any “dangling” nodes, i.e.,
##    nodes without any outedges. The dict key is the node the outedge points to and the dict
##    value is the weight of that outedge. By default, dangling nodes are given outedges according
##    to the personalization vector (uniform if not specified). This must be selected to result in
##    an irreducible transition matrix (see notes under google_matrix). It may be common to have the
##    dangling dict to be the same as the personalization dict.
########################################

########################################
## Import packages
########################################
import os
import json
import operator
import nltk
import pickle
import requests
import pandas as pd
import networkx as nx

########################################
## Make HTTP GET request on gitlab repos to recover all issues
########################################
def get_gitlab_repo_issues(username,repo_name,private_token):
    issues_json = []
    if not os.path.isfile('./' + repo_name + '_issues.json'):
        total_pages = requests.get('https://gitlab.com/api/v4/projects/' +  username
                                        + '%2F' + repo_name + '/issues?page=1'  +
                                        '&per_page=100&private_token=' + private_token,
                                        verify=True).headers['X-Total-Pages']
        for page_number in range(1, int(total_pages)):
            issues_json += requests.get('https://gitlab.com/api/v4/projects/' +  username
                                        + '%2F' + repo_name + '/issues?page=' + str(page_number) +
                                        '&per_page=100&private_token=' + private_token,
                                        verify=True).json()

        with open( repo_name + '_issues.json', 'w') as outfile:
            json.dump(issues_json, outfile)
    else:
        with open( repo_name +'_issues.json', 'r') as infile:
            issues_json = json.load(infile)
    return issues_json

########################################
## Make HTTP GET request on github repos to recover all issues
########################################
def get_github_repo_issues(username,repo_name,private_token):
    issues_json = []
    if not os.path.isfile('./' + repo_name + '_issues.json'):
        total_pages = requests.get('https://api.github.com/repos/'+ username +'/'+ repo_name +'/'
                                      +'issues?filter=all&state=all&page=' + str(page_number) +
                                        '&per_page=100&access_token=' + private_token,
                                        verify=True).headers['X-Total-Pages']
        for page_number in range(1, int(total_pages)):
            issues_json += requests.get('https://api.github.com/repos/'+ username +'/'+ repo_name +'/'
                                      +'issues?filter=all&state=all&page=' + str(page_number) +
                                        '&per_page=100&access_token=' + private_token,
                                        verify=True).json()
            with open(repo_name + '_issues.json', 'w') as outfile:
                json.dump(issues_json, outfile)
    else:
        with open(repo_name + '_issues.json', 'r') as infile:
            issues_json = json.load(infile)
    return issues_json


########################################
## Make HTTP GET request on gitlab repos to recover all comments from each issue
########################################
def get_gitlab_issues_comments(issue_id,username,repo_name,private_token):
    comments_json = []
    if not os.path.exists('./'+ repo_name +'_issues_comments'):
        os.makedirs('./'+ repo_name +'_issues_comments')

    if not os.path.isfile('./'+repo_name+'_issues_comments/issue_' + str(issue_id) + '_comments.json'):
        comments_json = requests.get('https://gitlab.com/api/v4/projects/' +  username
                                        + '%2F' + repo_name + '/issues/' + str(issue_id) +
                                        '/notes?&per_page=100&private_token=' + private_token,
                                     verify=True).json()
        with open('./' + repo_name + '_issues_comments/issue_' + str(issue_id) + '_comments.json', 'w') as outfile:
            json.dump(comments_json, outfile)
    else:
        with open('./' + repo_name + '_issues_comments/issue_' + str(issue_id) + '_comments.json', 'r') as infile:
            comments_json = json.load(infile)
    return comments_json


########################################
## Make HTTP GET request on github repos to recover all comments from each issue
########################################
def get_github_issues_comments(issue_id,username,repo_name,private_token):
    comments_json = []
    if not os.path.exists('./' + repo_name + '_issues_comments'):
        os.makedirs('./' + repo_name + '_issues_comments')
    if not os.path.isfile('./' + repo_name + '_issues_comments/issue_' + str(issue_id) + '_comments.json'):
        comments_json = requests.get('https://api.github.com/repos/' + username + '/' + repo_name
                                     + '/issues/' + str(issue_id) +
                                     '/comments?&per_page=100&access_token=' + private_token,
                                     verify=True).json()
        with open('./'+ repo_name + '_issues_comments/issue_' + str(issue_id) + '_comments.json', 'w') as outfile:
            json.dump(comments_json, outfile)
    else:
        with open('./' + repo_name + '_issues_comments/issue_' + str(issue_id) + '_comments.json', 'r') as infile:
            comments_json = json.load(infile)
    return comments_json



########################################
## Generates a dictionary where each key is a issue and the value is a list with the issues with inbound links from the key
########################################
def gen_gitlab_issue_dict(gitlab_issues,username,repo_name,private_token):
    issues = gitlab_issues
    issues_dict = {}
    for issue in issues:
        issues_dict[str(issue['iid'])] = []
        comments = get_gitlab_issues_comments(issue['iid'],username,repo_name,private_token)
        for comment in comments:
            comment_tokens = nltk.word_tokenize(comment['body'])
            for index, word in enumerate(comment_tokens):
                if index < (len(comment_tokens) - 1):
                    if word == '#' and comment_tokens[index + 1].isdigit():
                        issues_dict[str(issue['iid'])].append(comment_tokens[index + 1])
    return issues_dict


########################################
## Generates a dictionary where each key is a issue and the value is a list with the issues with inbound links from the key
########################################
def gen_github_issue_dict(github_issues,username,repo_name,private_token):
    issues = github_issues
    issues_dict = {}
    for issue in issues:
        issues_dict[str(issue['number'])] = []
        comments = get_github_issues_comments(issue['number'],username,repo_name,private_token)
        for comment in comments:
            comment_tokens = nltk.word_tokenize(comment['body'])
            for index, word in enumerate(comment_tokens):
                if index < (len(comment_tokens) - 1):
                    if word == '#' and comment_tokens[index + 1].isdigit():
                        issues_dict[str(issue['number'])].append(comment_tokens[index + 1])
    return issues_dict




########################################
## Generates a NetworkX DAG
########################################
def gen_graph(issues):
    graph = nx.DiGraph()
    issues_dict = issues
    for key in issues_dict:
        graph.add_node(key)
    for key, value in issues_dict.items():
        if isinstance(value, list):
            for node in value:
                graph.add_edge(key, node)
        else:
            graph.add_edge(key, value)
    return graph


########################################
## Runs the pagerank algorithm on the DAG from the issues
########################################
def run_pagerank(graph):
    page_rank = nx.pagerank(graph)
    return page_rank

########################################
## Sort a dictionary by the values
########################################
def sort_dict(dictionary):
    sorted_dict = sorted(dictionary.items(), key=operator.itemgetter(1))
    return sorted_dict

########################################
## Serialize and save dictionary for future use
########################################
def save_dict(obj,name):
    with open('./'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

########################################
## Clear given dict
########################################
def clear_dict(dictionary):
    dict_0 = dict((key, value) for key, value in dictionary.items() if value)
    return dict_0


########################################
## Main function
########################################
def main():
    gitservice = input('Are you using Github or Gitlab?')
    username = input('Git service username or organization associated with the repo:')
    repo_name = input('Repository name:')
    private_token = input('Your account private token:')

    if gitservice.lower() == 'github':
        issues = get_github_repo_issues(username,repo_name,private_token)
        issues_dict = gen_github_issue_dict(issues,username,repo_name,private_token)
        issues_dict = clear_dict(issues_dict)
        graph = gen_graph(issues_dict)
        nx.write_gpickle(graph, str(repo_name) + '.graph')
        pagerank = run_pagerank(graph)
        sorted_dict = sort_dict(pagerank)
        save_dict(sorted_dict, repo_name.lower())
    elif gitservice.lower() == 'gitlab':
        issues = get_gitlab_repo_issues(username,repo_name,private_token)
        issues_dict = gen_gitlab_issue_dict(issues,username,repo_name,private_token)
        issues_dict = clear_dict(issues_dict)
        graph = gen_graph(issues_dict)
        nx.write_gpickle(graph, str(repo_name) + '.graph')
        pagerank = run_pagerank(graph)
        sorted_dict = sort_dict(pagerank)
        save_dict(sorted_dict, repo_name.lower())
if __name__ == '__main__':
    main()
